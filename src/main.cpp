#include <iostream>
#include <vector>
#include <string>
#include "check_possible_movement.h"    //Kalle
#include "filefuncs.h" // Juho
#include "gameplay.h"   //Juho
#include "getPieces.h" // Karl
#include "initialBoard.h"   //Karl
#include "promotion.h"  //Karl
#include "showBoard.h"  //Karl
#include "attackedsquares.h" // Kalle, juho
#include "legal_destination.h"

std::string board[8][8];

std::vector<std::string> moves_vector;//game moves will be saved to here, or loaded to here
std::vector<std::vector<std::string>> save_holder;//holds the data from save file
std::string game_choice;
int save_slot;
int number_of_saves;
// as name states, for testing
std::vector<std::string> test_garbage_vector{"asd","qwe","ert","345","4fs"};
std::string test_garbage;


int turncount = 2;
bool game_on = true;
std::string turn_id;

std::string player_input;

int attackedBoardBlack[8][8] = {};
int attackedBoardWhite[8][8] = {};


int main(){

    number_of_saves = saveFileCheck();
    if (number_of_saves == 10){
        save_holder = loadFile();
    }else{
        save_holder = newFile();
    }

    initialBoard(board);

    std::cout<<"Select game mode\n1: New game\n2: Load game\n";

    std::cin>>game_choice;

    if(game_choice == "1"){
        // Start new game from fresh board

    }else if(game_choice == "2"){
        system("clear");
        // HERE PRINT GAME BOARD
        showBoard(board);
        // Load game from file
        std::cout<<"Select game file to load (0-9)\n";
        std::cin>>save_slot;

        moves_vector = save_holder[save_slot];



        for(int i=0; i < (int)moves_vector.size();i++){
            doMove(board, moves_vector[i]);
            system("clear");
        // HERE PRINT GAME BOARD
            showBoard(board);
            std::cout<<"press a key to continue\n";
            std::cin.get();
            turncount++;
        }
        //For loop to cycle through game loop

         std::cout<<"Loading finished, press a key to continue to progress to game\n";
         std::cin.get();

    }

    while(game_on){ // Gameplay loop is here

        // resetting check board and filling it with attackedsquares would come here
        // checking king status regarding attacks and moves would come here
    
        system("clear");
        // HERE PRINT GAME BOARD
        showBoard(board);

        turn_id = turn(turncount);//Get "W" or "B" depending on which color plays
        std::cout<<"Please input a move or quit to save and quit, concede to concede\n";
        

        std::cin>>player_input;
        


        if(player_input == "quit"){
            std::cout<<"Select game file to save (0-9) else skip save\n";
            std::cin>>save_slot;
            if(save_slot > 9 || save_slot < 0){//skips save if invalid number
                std::cout<<"Saving skipped, exiting program\n";
                return 0;
            }
            save_holder[save_slot] = moves_vector; //Specific slot gets overwritten by game progress
            saveGame(save_holder);
            std::cout<<"Saved to slot "<<save_slot<<"\n";
            return 0;


        }else if(player_input == "concede"){
            std::cout<<turn_id<<" gave up,congratz other player\n";
            std::cout<<"Select game file to save (0-9), else skip save\n";
            std::cin>>save_slot;
            if(save_slot > 9 || save_slot < 0){//skips save if invalid number
                std::cout<<"Saving skipped, exiting program\n";
                return 0;
            }
            save_holder[save_slot] = moves_vector;
            saveGame(save_holder);
            std::cout<<"Saved to slot "<<save_slot<<"\n";
            return 0;
        }
        
        int coordInt = inputToCoords(player_input);// converts string to 4 digit int
        
        if(coordInt == -1){
            
            std::cout<<"error at inputtocoords\n";
            std::cin.get();

            continue;
        }
        int originX = coordInt / 1000;// extract digits from 4 digit int
        int originY = (coordInt % 1000) / 100;
        int endX = (coordInt % 100) / 10;
        int endY = coordInt % 10;
        // for debugging purposes prints integers where things are supposed to happen
        std::cout<<originX<< " "<<originY<< " "<<endX<< " "<<endY<< "\n";
        std::string asd = board[originX][originY];//To print wich piece was targeted
        std::cout<<asd<<"\n";
        //chess_movement(originX, originY,board);

        if(legal_destination(originX,originY,endX,endY,board)){// IF move is legal, perform piece move
            doMove(board, player_input);
            //board[endX][endY] = board[originX][originY];
            //board[originX][originY] = " ";

        }else{
            std::cout<<"legaldestination\n";// to print if unintened stuff happens here

            continue;
            
        }

        moves_vector.push_back(player_input);// add the move string to vector to save later
        promotion(turncount, board);
        turncount++;
    }

        


    return 0;
}

