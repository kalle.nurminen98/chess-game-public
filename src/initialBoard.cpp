#include "initialBoard.h"

void initialBoard(std::string board[][8]){
    //bottom two rows
    board[0][0] = "WR"; board[1][0] = "WN"; board[2][0] = "WB"; board[3][0] = "WQ"; 
    board[4][0] = "WK"; board[5][0] = "WB"; board[6][0] = "WN"; board[7][0] = "WR";
    for(int i = 0; i < 8; i++){
        board[i][1] = "WP";
    }

    //for the four middle rows
    for(int i = 0; i < 8; i++){
        for(int j = 2; j < 6; j++){
           board[i][j] = " ";
        }
    }

    //top two rows
    board[0][7] = "BR"; board[1][7] = "BN"; board[2][7] = "BB"; board[3][7] = "BQ"; 
    board[4][7] = "BK"; board[5][7] = "BB"; board[6][7] = "BN"; board[7][7] = "BR";
    for(int i = 0; i < 8; i++){
        board[i][6] = "BP";
    }
}