#include "attackedsquares.h"

//Edited Kalle's function to mark every attacked square

//would fill an array to compare enemy king to, to determine checks and checkmates and stalemates


void attackedsquares(int x, int y, std::string board[][8], int attackedboard[8][8])
{
    if(board[x][y] == "WN")
    {
        int knight_X[8] = { 2, 1, -1, -2, -2, -1, 1, 2 };
        int knight_Y[8] = { 1, 2, 2, 1, -1, -2, -2, -1 };
        for(int i = 0; i < 8; i++)
        {
            int x_move = x + knight_X[i];
            int y_move = y + knight_Y[i];
            if(x_move >= 0 && y_move >= 0 && x_move < 8 && y_move < 8 && board[x_move][y_move] != "WP" && board[x_move][y_move] != "WR" && board[x_move][y_move] != "WN"
            && board[x_move][y_move] != "WB" && board[x_move][y_move] != "WQ" && board[x_move][y_move] != "WK")
            {
                attackedboard[x_move][y_move] = 1;
            }
            
        }
    }
    if(board[x][y] == "BN")
    {
        int knight_X[8] = { 2, 1, -1, -2, -2, -1, 1, 2 };
        int knight_Y[8] = { 1, 2, 2, 1, -1, -2, -2, -1 };
        for(int i = 0; i < 8; i++)
        {
            int x_move = x + knight_X[i];
            int y_move = y + knight_Y[i];
            if(x_move >= 0 && y_move >= 0 && x_move < 8 && y_move < 8 && board[x_move][y_move] != "BP" && board[x_move][y_move] != "BR" && board[x_move][y_move] != "BN"
            && board[x_move][y_move] != "BB" && board[x_move][y_move] != "BQ" && board[x_move][y_move] != "BK")
            {
                attackedboard[x_move][y_move] = 1;
            }
            
        }
    }
    if(board[x][y] == "WB")
    {
        int direction_x[4] = {-1,-1,1,1};
        int direction_y[4] = {-1,1,-1,1};
        int check = 0;
        for(int direction = 0; direction <4; direction++)
        {
            for(int i = 0; i < 8; i++)
            {
                int x_move = x + direction_x[direction]*i;
                int y_move = y + direction_y[direction]*i;
                if(x_move >= 0 && y_move >= 0 && x_move < 8 && y_move < 8 && board[x][y] != board[x_move][y_move] && board[x_move][y_move] != "WP" 
                && board[x_move][y_move] != "WR" && board[x_move][y_move] != "WN" && board[x_move][y_move] != "WN" && board[x_move][y_move] != "WQ" && board[x_move][y_move] != "WK")
                {
                    attackedboard[x_move][y_move] = 1;
                }
                if((x_move >= 0 && y_move >= 0 && x_move < 8 && y_move < 8 && board[x][y] != board[x_move][y_move]) && (board[x_move][y_move] == "WP"
                || board[x_move][y_move] == "WR" || board[x_move][y_move] == "WN" || board[x_move][y_move] == "WN" || board[x_move][y_move] == "WQ" || board[x_move][y_move] == "WK"))
                {
                    check = 1;
                    break;
                }
                if((x_move >= 0 && y_move >= 0 && x_move < 8 && y_move < 8 && board[x][y] != board[x_move][y_move]) && (board[x_move][y_move] == "BP"
                || board[x_move][y_move] == "BR" || board[x_move][y_move] == "BN" || board[x_move][y_move] == "BN" || board[x_move][y_move] == "BQ" || board[x_move][y_move] == "BK"))
                {
                    check = 1;
                    break;
                }
              
            }
        }
        if( check == 1)
        {
//            std::cout << "No possible solutions anymore" << std::endl;
        } 
    }
    if(board[x][y] == "BB")
    {
        int direction_x[4] = {-1,-1,1,1};
        int direction_y[4] = {-1,1,-1,1};
        int check = 0;
        for(int direction = 0; direction <4; direction++)
        {
            for(int i = 0; i < 8; i++)
            {
                int x_move = x + direction_x[direction]*i;
                int y_move = y + direction_y[direction]*i;
                if(x_move >= 0 && y_move >= 0 && x_move < 8 && y_move < 8 && board[x][y] != board[x_move][y_move] && board[x_move][y_move] != "BP" 
                && board[x_move][y_move] != "BR" && board[x_move][y_move] != "BN" && board[x_move][y_move] != "BN" && board[x_move][y_move] != "BQ" && board[x_move][y_move] != "BK")
                {
                    attackedboard[x_move][y_move] = 1;
                }
                if((x_move >= 0 && y_move >= 0 && x_move < 8 && y_move < 8 && board[x][y] != board[x_move][y_move]) && (board[x_move][y_move] == "BP"
                || board[x_move][y_move] == "BR" || board[x_move][y_move] == "BN" || board[x_move][y_move] == "BN" || board[x_move][y_move] == "BQ" || board[x_move][y_move] == "BK"))
                {
                    check = 1;
                    break;
                }
                if((x_move >= 0 && y_move >= 0 && x_move < 8 && y_move < 8 && board[x][y] != board[x_move][y_move]) && (board[x_move][y_move] == "WP"
                || board[x_move][y_move] == "WR" || board[x_move][y_move] == "WN" || board[x_move][y_move] == "WN" || board[x_move][y_move] == "WQ" || board[x_move][y_move] == "WK"))
                {
                    check = 1;
                    break;
                }           
            }
        }
        if( check == 1)
        {
//            std::cout << "No possible solutions anymore" << std::endl;
        } 
    }
    if(board[x][y] == "WR")
    {
        int direction_x[4] = {-1,1,0,0};
        int direction_y[4] = {0,0,-1,1};
        int check = 0;
        for(int direction = 0; direction <4; direction++)
        {
            for(int i = 0; i < 8; i++)
            {
                int x_move = x + direction_x[direction]*i;
                int y_move = y + direction_y[direction]*i;
                if(x_move >= 0 && y_move >= 0 && x_move < 8 && y_move < 8 && board[x][y] != board[x_move][y_move] && board[x_move][y_move] != "WP" 
                && board[x_move][y_move] != "WB" && board[x_move][y_move] != "WN" && board[x_move][y_move] != "WN" && board[x_move][y_move] != "WQ" && board[x_move][y_move] != "WK")          
                {
                    attackedboard[x_move][y_move] = 1;
                }
                if((x_move >= 0 && y_move >= 0 && x_move < 8 && y_move < 8 && board[x][y] != board[x_move][y_move]) && (board[x_move][y_move] == "WP"
                || board[x_move][y_move] == "WB" || board[x_move][y_move] == "WN" || board[x_move][y_move] == "WN" || board[x_move][y_move] == "WQ" || board[x_move][y_move] == "WK"))
                {
                    check = 1;
                    break;
                }
                if((x_move >= 0 && y_move >= 0 && x_move < 8 && y_move < 8 && board[x][y] != board[x_move][y_move]) && (board[x_move][y_move] == "BP"
                || board[x_move][y_move] == "BB" || board[x_move][y_move] == "BN" || board[x_move][y_move] == "BN" || board[x_move][y_move] == "BQ" || board[x_move][y_move] == "BK"))
                {
                    check = 1;
                    break;
                }
              
            }
        }
        if( check == 1)
        {
//            std::cout << "No possible solutions anymore" << std::endl;
        }    
    }
    if(board[x][y] == "BR")
    {
        int direction_x[4] = {-1,1,0,0};
        int direction_y[4] = {0,0,-1,1};
        int check = 0;
        for(int direction = 0; direction <4; direction++)
        {
            for(int i = 0; i < 8; i++)
            {
                int x_move = x + direction_x[direction]*i;
                int y_move = y + direction_y[direction]*i;
                if(x_move >= 0 && y_move >= 0 && x_move < 8 && y_move < 8 && board[x][y] != board[x_move][y_move] && board[x_move][y_move] != "BP" 
                && board[x_move][y_move] != "BB" && board[x_move][y_move] != "BN" && board[x_move][y_move] != "BN" && board[x_move][y_move] != "BQ" && board[x_move][y_move] != "BK")          
                {
                    attackedboard[x_move][y_move] = 1;
                }
                if((x_move >= 0 && y_move >= 0 && x_move < 8 && y_move < 8 && board[x][y] != board[x_move][y_move]) && (board[x_move][y_move] == "BP"
                || board[x_move][y_move] == "BB" || board[x_move][y_move] == "BN" || board[x_move][y_move] == "BN" || board[x_move][y_move] == "BQ" || board[x_move][y_move] == "BK"))
                {
                    check = 1;
                    break;
                }
                if((x_move >= 0 && y_move >= 0 && x_move < 8 && y_move < 8 && board[x][y] != board[x_move][y_move]) && (board[x_move][y_move] == "WP"
                || board[x_move][y_move] == "WB" || board[x_move][y_move] == "WN" || board[x_move][y_move] == "WN" || board[x_move][y_move] == "WQ" || board[x_move][y_move] == "WK"))
                {
                    check = 1;
                    break;
                }           
            }
        }
        if( check == 1)
        {
//            std::cout << "No possible solutions anymore" << std::endl;
        } 
    }
    if(board[x][y] == "WK")
    {
        int direction_x[8] = {-1,1,0,0,-1,-1,1,1};
        int direction_y[8] = {0,0,-1,1,-1,1,-1,1};
        int check = 0;
        for(int direction = 0; direction <8; direction++)
        {
            int x_move = x + direction_x[direction];
            int y_move = y + direction_y[direction];

            if(x_move >= 0 && y_move >= 0 && x_move < 8 && y_move < 8 && board[x][y] != board[x_move][y_move] && board[x_move][y_move] != "WP" 
            && board[x_move][y_move] != "WB" && board[x_move][y_move] != "WN" && board[x_move][y_move] != "WN" && board[x_move][y_move] != "WQ" && board[x_move][y_move] != "WR")          
            {
                attackedboard[x_move][y_move] = 1;
            }
            if((x_move >= 0 && y_move >= 0 && x_move < 8 && y_move < 8 && board[x][y] != board[x_move][y_move]) && (board[x_move][y_move] == "WP"
            || board[x_move][y_move] == "WB" || board[x_move][y_move] == "WN" || board[x_move][y_move] == "WN" || board[x_move][y_move] == "WQ" || board[x_move][y_move] == "WR"))
            {
                check = 1;
            }
            if((x_move >= 0 && y_move >= 0 && x_move < 8 && y_move < 8 && board[x][y] != board[x_move][y_move]) && (board[x_move][y_move] == "BP"
            || board[x_move][y_move] == "BB" || board[x_move][y_move] == "BN" || board[x_move][y_move] == "BN" || board[x_move][y_move] == "BQ" || board[x_move][y_move] == "BR"))
            {
                check = 1;
            }
          
        }
        if( check == 1)
        {
//            std::cout << "No possible solutions anymore" << std::endl;
        } 
        
    }

    if(board[x][y] == "BK")
    {
        int direction_x[8] = {-1,1,0,0,-1,-1,1,1};
        int direction_y[8] = {0,0,-1,1,-1,1,-1,1};
        int check = 0;
        for(int direction = 0; direction <8; direction++)
        {
            int x_move = x + direction_x[direction];
            int y_move = y + direction_y[direction];

            if(x_move >= 0 && y_move >= 0 && x_move < 8 && y_move < 8 && board[x][y] != board[x_move][y_move] && board[x_move][y_move] != "BP" 
            && board[x_move][y_move] != "BB" && board[x_move][y_move] != "BN" && board[x_move][y_move] != "BN" && board[x_move][y_move] != "BQ" && board[x_move][y_move] != "BR")          
            {
                attackedboard[x_move][y_move] = 1;
            }
            if((x_move >= 0 && y_move >= 0 && x_move < 8 && y_move < 8 && board[x][y] != board[x_move][y_move]) && (board[x_move][y_move] == "BP"
            || board[x_move][y_move] == "BB" || board[x_move][y_move] == "BN" || board[x_move][y_move] == "BN" || board[x_move][y_move] == "BQ" || board[x_move][y_move] == "BR"))
            {
                check = 1;
            }
            if((x_move >= 0 && y_move >= 0 && x_move < 8 && y_move < 8 && board[x][y] != board[x_move][y_move]) && (board[x_move][y_move] == "WP"
            || board[x_move][y_move] == "WB" || board[x_move][y_move] == "WN" || board[x_move][y_move] == "WN" || board[x_move][y_move] == "WQ" || board[x_move][y_move] == "WR"))
            {
                check = 1;
            }
          
        }
        if( check == 1)
        {
//            std::cout << "No possible solutions anymore" << std::endl;
        } 
        
    }
    if(board[x][y] == "WQ")
    {
        int direction_x[8] = {-1,1,0,0,-1,-1,1,1};
        int direction_y[8] = {0,0,-1,1,-1,1,-1,1};
        int check = 0;
        for(int direction = 0; direction <8; direction++)
        {
            for(int i = 0; i < 8; i++)
            {
                int x_move = x + direction_x[direction]*i;
                int y_move = y + direction_y[direction]*i;
                if(x_move >= 0 && y_move >= 0 && x_move < 8 && y_move < 8 && board[x][y] != board[x_move][y_move] && board[x_move][y_move] != "WP" 
                && board[x_move][y_move] != "WB" && board[x_move][y_move] != "WN" && board[x_move][y_move] != "WN" && board[x_move][y_move] != "WR" && board[x_move][y_move] != "WK")          
                {
                    attackedboard[x_move][y_move] = 1;
                }
                if((x_move >= 0 && y_move >= 0 && x_move < 8 && y_move < 8 && board[x][y] != board[x_move][y_move]) && (board[x_move][y_move] == "WP"
                || board[x_move][y_move] == "WB" || board[x_move][y_move] == "WN" || board[x_move][y_move] == "WN" || board[x_move][y_move] == "WR" || board[x_move][y_move] == "WK"))
                {
                    check = 1;
                    break;
                }
                if((x_move >= 0 && y_move >= 0 && x_move < 8 && y_move < 8 && board[x][y] != board[x_move][y_move]) && (board[x_move][y_move] == "BP"
                || board[x_move][y_move] == "BB" || board[x_move][y_move] == "BN" || board[x_move][y_move] == "BN" || board[x_move][y_move] == "BR" || board[x_move][y_move] == "BK"))
                {
                    check = 1;
                    break;
                }
              
            }
        }
        if( check == 1)
        {
//           std::cout << "No possible solutions anymore" << std::endl;
        } 
    }

    if(board[x][y] == "BQ")
    {
        int direction_x[8] = {-1,1,0,0,-1,-1,1,1};
        int direction_y[8] = {0,0,-1,1,-1,1,-1,1};
        int check = 0;
        for(int direction = 0; direction <8; direction++)
        {
            for(int i = 0; i < 8; i++)
            {
                int x_move = x + direction_x[direction]*i;
                int y_move = y + direction_y[direction]*i;
                if(x_move >= 0 && y_move >= 0 && x_move < 8 && y_move < 8 && board[x][y] != board[x_move][y_move] && board[x_move][y_move] != "BP" 
                && board[x_move][y_move] != "BB" && board[x_move][y_move] != "BN" && board[x_move][y_move] != "BN" && board[x_move][y_move] != "BR" && board[x_move][y_move] != "BK")          
                {
                    attackedboard[x_move][y_move] = 1;
                }
                if((x_move >= 0 && y_move >= 0 && x_move < 8 && y_move < 8 && board[x][y] != board[x_move][y_move]) && (board[x_move][y_move] == "BP"
                || board[x_move][y_move] == "BB" || board[x_move][y_move] == "BN" || board[x_move][y_move] == "BN" || board[x_move][y_move] == "BR" || board[x_move][y_move] == "BK"))
                {
                    check = 1;
                    break;
                }
                if((x_move >= 0 && y_move >= 0 && x_move < 8 && y_move < 8 && board[x][y] != board[x_move][y_move]) && (board[x_move][y_move] == "WP"
                || board[x_move][y_move] == "WB" || board[x_move][y_move] == "WN" || board[x_move][y_move] == "WN" || board[x_move][y_move] == "WR" || board[x_move][y_move] == "WK"))
                {
                    check = 1;
                    break;
                }
              
            }
        }
        if( check == 1)
        {
            std::cout << "No possible solutions anymore" << std::endl;
        } 
    }
    if(board[x][y] == "WP")
    {
        int check = 0;
        // if(x == 1)
        // {
        //     std::cout << "Possible destinations (" << x+2 << "," << y <<")" << std::endl;
        // }
        // if((x > 0 && y > 0 && x < 7 && y< 8) && board[x][y-1] == "BP") 
        // {
        //     std::cout << "Possible en passant (" << x+1 << "," << y-1 <<")" << std::endl;
        // }
        // if((x > 0 && y > 0 && x < 7 && y< 8) && board[x][y+1] == "BP") 
        // {
        //     std::cout << "Possible en passant (" << x+1 << "," << y+1 <<")" << std::endl;
        // }
        if((board[x+1][y] == "BP" || board[x+1][y] == "BB"
        || board[x+1][y] == "BN" || board[x+1][y] == "BQ" || board[x+1][y] == "BR" || board[x+1][y] == "BK" ))
        {
            check = 1;
        }
        if((x >= 0 && y >= 0 && x < 7 && y< 8) && (board[x+1][y+1] == "BP" || board[x+1][y+1] == "BB"
        || board[x+1][y+1] == "BN" || board[x+1][y+1] == "BQ" || board[x+1][y+1] == "BR" || board[x+1][y+1] == "BK" ))
        {
            attackedboard[x+1][y+1] = 1;
        }
        if((x > 0 && y > 0 && x < 7 && y< 8) && (board[x+1][y-1] == "BP" || board[x+1][y-1] == "BB"
        || board[x+1][y-1] == "BN" || board[x+1][y-1] == "BQ" || board[x+1][y-1] == "BR" || board[x+1][y-1] == "BK" ))
        {
            attackedboard[x-1][y-1] = 1;
        }
        if(check == 0)
        {
//            std::cout << "Possible destinations (" << x+1 << "," << y <<")" << std::endl;
        }
 
    }
    if(board[x][y] == "BP")
    {
        int check = 0;
//         if(x == 6)
//         {
//            std::cout << "Possible destinations (" << x-2 << "," << y <<")" << std::endl;
//         }
//         if((x > 0 && y > 0 && x < 7 && y< 8) && board[x][y+1] == "WP") 
//         {
//            std::cout << "Possible en passant (" << x-1 << "," << y+1 <<")" << std::endl;
//         }
//         if((x > 0 && y > 0 && x < 7 && y< 8) && board[x][y-1] == "WP") 
//         {
//            std::cout << "Possible en passant (" << x-1 << "," << y-1 <<")" << std::endl;
//         }
        if((board[x-1][y] == "WP" || board[x-1][y] == "WB"
        || board[x-1][y] == "WN" || board[x-1][y] == "WQ" || board[x-1][y] == "WR" || board[x-1][y] == "WK" ))
        {
            check = 1;
        }
        if((x >= 0 && y >= 0 && x < 7 && y< 8) && (board[x-1][y-1] == "WP" || board[x-1][y-1] == "WB"
        || board[x-1][y-1] == "WN" || board[x-1][y-1] == "WQ" || board[x-1][y-1] == "WR" || board[x-1][y-1] == "WK" ))
        {
            attackedboard[x-1][y-1] = 1;
        }
        if((x > 0 && y > 0 && x < 7 && y< 8) && (board[x-1][y+1] == "WP" || board[x-1][y+1] == "WB"
        || board[x-1][y+1] == "WN" || board[x-1][y+1] == "WQ" || board[x-1][y+1] == "WR" || board[x-1][y+1] == "WK" ))
        {
            attackedboard[x-1][y+1] = 1;
        }
        if(check == 0)
        {
//            std::cout << "Possible destinations (" << x-1 << "," << y <<")" << std::endl;
        }
 
    }
    
}