#include "legal_destination.h"

// This function supposedly determines if certain move is allowed

bool legal_destination(int x, int y, int x_end, int y_end,std::string board[][8])
{
    // If user coordinates have white knight
    if(board[x][y] == "WN")
    {
        //All knight possible movements
        int knight_X[8] = { 2, 1, -1, -2, -2, -1, 1, 2 };
        int knight_Y[8] = { 1, 2, 2, 1, -1, -2, -2, -1 };
        // For loop for all those movement options
        for(int i = 0; i < 8; i++)
        {
            int x_move = x + knight_X[i];
            int y_move = y + knight_Y[i];
            // If legal movement is possible
            if(x_move >= 0 && y_move >= 0 && x_move < 8 && y_move < 8 && board[x_move][y_move] != "WP" && board[x_move][y_move] != "WR" && board[x_move][y_move] != "WN"
            && board[x_move][y_move] != "WB" && board[x_move][y_move] != "WQ" && board[x_move][y_move] != "WK")
            {
                // If user destination coordinates are possible then return True
                if(x_end == x_move && y_end == y_move)
                {
                    return true;
                }
            }
        }
    }
    // If user coordinates have black knight
    if(board[x][y] == "BN")
    {
        //All knight possible movements
        int knight_X[8] = { 2, 1, -1, -2, -2, -1, 1, 2 };
        int knight_Y[8] = { 1, 2, 2, 1, -1, -2, -2, -1 };
        // For loop for all those movement options
        for(int i = 0; i < 8; i++)
        {
            int x_move = x + knight_X[i];
            int y_move = y + knight_Y[i];
            // If legal movement is possible
            if(x_move >= 0 && y_move >= 0 && x_move < 8 && y_move < 8 && board[x_move][y_move] != "BP" && board[x_move][y_move] != "BR" && board[x_move][y_move] != "BN"
            && board[x_move][y_move] != "BB" && board[x_move][y_move] != "BQ" && board[x_move][y_move] != "BK")
            {
                // If user destination coordinates are possible then return True
                if(x_end == x_move && y_end == y_move)
                {
                    return true;
                }
            }
            
        }
    }
    // If user coordinates have white bishop
    if(board[x][y] == "WB")
    {
        // All direction options
        int direction_x[4] = {-1,-1,1,1};
        int direction_y[4] = {-1,1,-1,1};
        int check = 0;
        // For loop for all directions
        for(int direction = 0; direction <4; direction++)
        {
            // For loop for all coordinates
            for(int i = 0; i < 8; i++)
            {
                int x_move = x + direction_x[direction]*i;
                int y_move = y + direction_y[direction]*i;
                // If legal movement is possible
                if(x_move >= 0 && y_move >= 0 && x_move < 8 && y_move < 8 && board[x][y] != board[x_move][y_move] && board[x_move][y_move] != "WP" 
                && board[x_move][y_move] != "WR" && board[x_move][y_move] != "WN" && board[x_move][y_move] != "WN" && board[x_move][y_move] != "WQ" && board[x_move][y_move] != "WK")
                {
                    // If user destination coordinates are possible then return True
                    if(x_end == x_move && y_end == y_move)
                    {
                        return true;
                    }
                }
                // If there is white game piece in front, then can't go further from it
                if((x_move >= 0 && y_move >= 0 && x_move < 8 && y_move < 8 && board[x][y] != board[x_move][y_move]) && (board[x_move][y_move] == "WP"
                || board[x_move][y_move] == "WR" || board[x_move][y_move] == "WN" || board[x_move][y_move] == "WN" || board[x_move][y_move] == "WQ" || board[x_move][y_move] == "WK"))
                {
                    check = 1;
                    // If user destination coordinates are possible then return True
                    if(x_end == x_move && y_end == y_move)
                    {
                        return true;
                    }
                    break;
                }
                // If there is black game piece in front, then you can eat it but not teleport past it
                if((x_move >= 0 && y_move >= 0 && x_move < 8 && y_move < 8 && board[x][y] != board[x_move][y_move]) && (board[x_move][y_move] == "BP"
                || board[x_move][y_move] == "BR" || board[x_move][y_move] == "BN" || board[x_move][y_move] == "BN" || board[x_move][y_move] == "BQ" || board[x_move][y_move] == "BK"))
                {
                    check = 1;
                    // If user destination coordinates are possible then return True
                    if(x_end == x_move && y_end == y_move)
                    {
                        return true;
                    }
                    break;
                }
              
            }
        }
        // Can't go further to that direction
        if( check == 1)
        {
            std::cout << "No possible solutions anymore" << std::endl;
        } 
    }
    // If user coordinates have black bishop
    if(board[x][y] == "BB")
    {
        // All direction options
        int direction_x[4] = {-1,-1,1,1};
        int direction_y[4] = {-1,1,-1,1};
        int check = 0;
        // For loop for all directions
        for(int direction = 0; direction <4; direction++)
        {
            // For loop for all coordinates
            for(int i = 0; i < 8; i++)
            {
                int x_move = x + direction_x[direction]*i;
                int y_move = y + direction_y[direction]*i;
                // If legal movement is possible
                if(x_move >= 0 && y_move >= 0 && x_move < 8 && y_move < 8 && board[x][y] != board[x_move][y_move] && board[x_move][y_move] != "BP" 
                && board[x_move][y_move] != "BR" && board[x_move][y_move] != "BN" && board[x_move][y_move] != "BN" && board[x_move][y_move] != "BQ" && board[x_move][y_move] != "BK")
                {
                    // If user destination coordinates are possible then return True
                    if(x_end == x_move && y_end == y_move)
                    {
                        return true;
                    }
                }
                // If there is black game piece in front, then can't go further from it
                if((x_move >= 0 && y_move >= 0 && x_move < 8 && y_move < 8 && board[x][y] != board[x_move][y_move]) && (board[x_move][y_move] == "BP"
                || board[x_move][y_move] == "BR" || board[x_move][y_move] == "BN" || board[x_move][y_move] == "BN" || board[x_move][y_move] == "BQ" || board[x_move][y_move] == "BK"))
                {
                    check = 1;
                    // If user destination coordinates are possible then return True
                    if(x_end == x_move && y_end == y_move)
                    {
                        return true;
                    }
                    break;
                }
                // If there is white game piece in front, then you can eat it but not teleport past it
                if((x_move >= 0 && y_move >= 0 && x_move < 8 && y_move < 8 && board[x][y] != board[x_move][y_move]) && (board[x_move][y_move] == "WP"
                || board[x_move][y_move] == "WR" || board[x_move][y_move] == "WN" || board[x_move][y_move] == "WN" || board[x_move][y_move] == "WQ" || board[x_move][y_move] == "WK"))
                {
                    check = 1;
                    // If user destination coordinates are possible then return True
                    if(x_end == x_move && y_end == y_move)
                    {
                        return true;
                    }
                    break;
                }           
            }
        }
        // Can't go further to that direction
        if( check == 1)
        {
            std::cout << "No possible solutions anymore" << std::endl;
        } 
    }
    // If user coordinates have white rook
    if(board[x][y] == "WR")
    {
        // All direction options
        int direction_x[4] = {-1,1,0,0};
        int direction_y[4] = {0,0,-1,1};
        int check = 0;
        // For loop for all directions
        for(int direction = 0; direction <4; direction++)
        {
            // For loop for all coordinates
            for(int i = 0; i < 8; i++)
            {
                int x_move = x + direction_x[direction]*i;
                int y_move = y + direction_y[direction]*i;
                // If legal movement is possible
                if(x_move >= 0 && y_move >= 0 && x_move < 8 && y_move < 8 && board[x][y] != board[x_move][y_move] && board[x_move][y_move] != "WP" 
                && board[x_move][y_move] != "WB" && board[x_move][y_move] != "WN" && board[x_move][y_move] != "WN" && board[x_move][y_move] != "WQ" && board[x_move][y_move] != "WK")          
                {
                    // If user destination coordinates are possible then return True
                    if(x_end == x_move && y_end == y_move)
                    {
                        return true;
                    }
                }
                // If there is white game piece in front, then can't go further from it
                if((x_move >= 0 && y_move >= 0 && x_move < 8 && y_move < 8 && board[x][y] != board[x_move][y_move]) && (board[x_move][y_move] == "WP"
                || board[x_move][y_move] == "WB" || board[x_move][y_move] == "WN" || board[x_move][y_move] == "WN" || board[x_move][y_move] == "WQ" || board[x_move][y_move] == "WK"))
                {
                    check = 1;
                    // If user destination coordinates are possible then return True
                    if(x_end == x_move && y_end == y_move)
                    {
                        return true;
                    }
                    break;
                }
                // If there is black game piece in front, then you can eat it but not teleport past it
                if((x_move >= 0 && y_move >= 0 && x_move < 8 && y_move < 8 && board[x][y] != board[x_move][y_move]) && (board[x_move][y_move] == "BP"
                || board[x_move][y_move] == "BB" || board[x_move][y_move] == "BN" || board[x_move][y_move] == "BN" || board[x_move][y_move] == "BQ" || board[x_move][y_move] == "BK"))
                {
                    check = 1;
                    // If user destination coordinates are possible then return True
                    if(x_end == x_move && y_end == y_move)
                    {
                        return true;
                    }
                    break;
                }
              
            }
        }
        // Can't go further to that direction
        if( check == 1)
        {
            std::cout << "No possible solutions anymore" << std::endl;
        }    
    }
    // If user coordinates have black rook
    if(board[x][y] == "BR")
    {
        // All direction options
        int direction_x[4] = {-1,1,0,0};
        int direction_y[4] = {0,0,-1,1};
        int check = 0;
        // For loop for all directions
        for(int direction = 0; direction <4; direction++)
        {
            // For loop for all coordinates
            for(int i = 0; i < 8; i++)
            {
                int x_move = x + direction_x[direction]*i;
                int y_move = y + direction_y[direction]*i;
                // If legal movement is possible
                if(x_move >= 0 && y_move >= 0 && x_move < 8 && y_move < 8 && board[x][y] != board[x_move][y_move] && board[x_move][y_move] != "BP" 
                && board[x_move][y_move] != "BB" && board[x_move][y_move] != "BN" && board[x_move][y_move] != "BN" && board[x_move][y_move] != "BQ" && board[x_move][y_move] != "BK")          
                {
                    // If user destination coordinates are possible then return True
                    if(x_end == x_move && y_end == y_move)
                    {
                        return true;
                    }
                }
                // If there is black game piece in front, then can't go further from it
                if((x_move >= 0 && y_move >= 0 && x_move < 8 && y_move < 8 && board[x][y] != board[x_move][y_move]) && (board[x_move][y_move] == "BP"
                || board[x_move][y_move] == "BB" || board[x_move][y_move] == "BN" || board[x_move][y_move] == "BN" || board[x_move][y_move] == "BQ" || board[x_move][y_move] == "BK"))
                {
                    check = 1;
                    // If user destination coordinates are possible then return True
                    if(x_end == x_move && y_end == y_move)
                    {
                        return true;
                    }
                    break;
                }
                // If there is white game piece in front, then you can eat it but not teleport past it
                if((x_move >= 0 && y_move >= 0 && x_move < 8 && y_move < 8 && board[x][y] != board[x_move][y_move]) && (board[x_move][y_move] == "WP"
                || board[x_move][y_move] == "WB" || board[x_move][y_move] == "WN" || board[x_move][y_move] == "WN" || board[x_move][y_move] == "WQ" || board[x_move][y_move] == "WK"))
                {
                    check = 1;
                    // If user destination coordinates are possible then return True
                    if(x_end == x_move && y_end == y_move)
                    {
                        return true;
                    }
                    break;
                }           
            }
        }
        // Can't go further to that direction
        if( check == 1)
        {
            std::cout << "No possible solutions anymore" << std::endl;
        } 
    }
    // If user coordinates have white king
    if(board[x][y] == "WK")
    {
        // All direction options
        int direction_x[8] = {-1,1,0,0,-1,-1,1,1};
        int direction_y[8] = {0,0,-1,1,-1,1,-1,1};
        int check = 0;
        // For loop for all directions
        for(int direction = 0; direction <8; direction++)
        {
            int x_move = x + direction_x[direction];
            int y_move = y + direction_y[direction];
            // If legal movement is possible
            if(x_move >= 0 && y_move >= 0 && x_move < 8 && y_move < 8 && board[x][y] != board[x_move][y_move] && board[x_move][y_move] != "WP" 
            && board[x_move][y_move] != "WB" && board[x_move][y_move] != "WN" && board[x_move][y_move] != "WN" && board[x_move][y_move] != "WQ" && board[x_move][y_move] != "WR")          
            {
                // If user destination coordinates are possible then return True
                if(x_end == x_move && y_end == y_move)
                {
                    return true;
                }
            }
            // If there is white game piece in front, then can't go further from it
            if((x_move >= 0 && y_move >= 0 && x_move < 8 && y_move < 8 && board[x][y] != board[x_move][y_move]) && (board[x_move][y_move] == "WP"
            || board[x_move][y_move] == "WB" || board[x_move][y_move] == "WN" || board[x_move][y_move] == "WN" || board[x_move][y_move] == "WQ" || board[x_move][y_move] == "WR"))
            {
                check = 1;
                // If user destination coordinates are possible then return True
                if(x_end == x_move && y_end == y_move)
                {
                    return true;
                }
            }
            // If there is black game piece in front, then you can eat it
            if((x_move >= 0 && y_move >= 0 && x_move < 8 && y_move < 8 && board[x][y] != board[x_move][y_move]) && (board[x_move][y_move] == "BP"
            || board[x_move][y_move] == "BB" || board[x_move][y_move] == "BN" || board[x_move][y_move] == "BN" || board[x_move][y_move] == "BQ" || board[x_move][y_move] == "BR"))
            {
                check = 1;
                // If user destination coordinates are possible then return True
                if(x_end == x_move && y_end == y_move)
                {
                    return true;
                }
            }
          
        }
        // Can't go further to that direction
        if( check == 1)
        {
            std::cout << "No possible solutions anymore" << std::endl;
        } 
        
    }
    // If user coordinates have black king
    if(board[x][y] == "BK")
    {
        // All direction options
        int direction_x[8] = {-1,1,0,0,-1,-1,1,1};
        int direction_y[8] = {0,0,-1,1,-1,1,-1,1};
        int check = 0;
        // For loop for all directions
        for(int direction = 0; direction <8; direction++)
        {
            int x_move = x + direction_x[direction];
            int y_move = y + direction_y[direction];
            // If legal movement is possible
            if(x_move >= 0 && y_move >= 0 && x_move < 8 && y_move < 8 && board[x][y] != board[x_move][y_move] && board[x_move][y_move] != "BP" 
            && board[x_move][y_move] != "BB" && board[x_move][y_move] != "BN" && board[x_move][y_move] != "BN" && board[x_move][y_move] != "BQ" && board[x_move][y_move] != "BR")          
            {
                // If user destination coordinates are possible then return True
                if(x_end == x_move && y_end == y_move)
                {
                    return true;
                }
            }
            // If there is black game piece in front, then can't go further from it
            if((x_move >= 0 && y_move >= 0 && x_move < 8 && y_move < 8 && board[x][y] != board[x_move][y_move]) && (board[x_move][y_move] == "BP"
            || board[x_move][y_move] == "BB" || board[x_move][y_move] == "BN" || board[x_move][y_move] == "BN" || board[x_move][y_move] == "BQ" || board[x_move][y_move] == "BR"))
            {
               
                check = 1;
                // If user destination coordinates are possible then return True
                if(x_end == x_move && y_end == y_move)
                {
                    return true;
                }
            }
            // If there is white game piece in front, then you can eat it
            if((x_move >= 0 && y_move >= 0 && x_move < 8 && y_move < 8 && board[x][y] != board[x_move][y_move]) && (board[x_move][y_move] == "WP"
            || board[x_move][y_move] == "WB" || board[x_move][y_move] == "WN" || board[x_move][y_move] == "WN" || board[x_move][y_move] == "WQ" || board[x_move][y_move] == "WR"))
            {
                // If user destination coordinates are possible then return True
                if(x_end == x_move && y_end == y_move)
                {
                    return true;
                }
                check = 1;
            }
          
        }
        // Can't go further to that direction
        if( check == 1)
        {
            std::cout << "No possible solutions anymore" << std::endl;
        } 
        
    }
    // If user coordinates have white queen
    if(board[x][y] == "WQ")
    {
        // All direction options
        int direction_x[8] = {-1,1,0,0,-1,-1,1,1};
        int direction_y[8] = {0,0,-1,1,-1,1,-1,1};
        int check = 0;
        // For loop for all directions
        for(int direction = 0; direction <8; direction++)
        {
            // For loop for all coordinates
            for(int i = 0; i < 8; i++)
            {
                int x_move = x + direction_x[direction]*i;
                int y_move = y + direction_y[direction]*i;
                // If legal movement is possible
                if(x_move >= 0 && y_move >= 0 && x_move < 8 && y_move < 8 && board[x][y] != board[x_move][y_move] && board[x_end][y_end] != "WP" 
                && board[x_end][y_end] != "WB" && board[x_end][y_end] != "WN" && board[x_end][y_end] != "WN" && board[x_end][y_end] != "WR" && board[x_end][y_end] != "WK")          
                {
                    // If user destination coordinates are possible then return True
                    if(x_end == x_move && y_end == y_move)
                    {
                        return true;
                    }
                }
                // If there is white game piece in front, then can't go further from it
                if((x_move >= 0 && y_move >= 0 && x_move < 8 && y_move < 8 && board[x][y] != board[x_move][y_move]) && (board[x_move][y_move] == "WP"
                || board[x_move][y_move] == "WB" || board[x_move][y_move] == "WN" || board[x_move][y_move] == "WN" || board[x_move][y_move] == "WR" || board[x_move][y_move] == "WK"))
                {
                    check = 1;
                    // If user destination coordinates are possible then return True
                    if(x_end == x_move && y_end == y_move)
                    {
                        return true;
                    }
                    break;
                }
                // If there is black game piece in front, then you can eat it but not teleport past it
                if((x_move >= 0 && y_move >= 0 && x_move < 8 && y_move < 8 && board[x][y] != board[x_move][y_move]) && (board[x_move][y_move] == "BP"
                || board[x_move][y_move] == "BB" || board[x_move][y_move] == "BN" || board[x_move][y_move] == "BN" || board[x_move][y_move] == "BR" || board[x_move][y_move] == "BK"))
                {
                    check = 1;
                    // If user destination coordinates are possible then return True
                    if(x_end == x_move && y_end == y_move)
                    {
                        return true;
                    }
                    break;
                }
              
            }
        }
        // Can't go further to that direction
        if( check == 1)
        {
            std::cout << "No possible solutions anymore" << std::endl;
        } 
    }
    // If user coordinates have black queen
    if(board[x][y] == "BQ")
    {
        // All direction options
        int direction_x[8] = {-1,1,0,0,-1,-1,1,1};
        int direction_y[8] = {0,0,-1,1,-1,1,-1,1};
        int check = 0;
        // For loop for all directions
        for(int direction = 0; direction <8; direction++)
        {
            // For loop for all coordinates
            for(int i = 0; i < 8; i++)
            {
                int x_move = x + direction_x[direction]*i;
                int y_move = y + direction_y[direction]*i;
                // If legal movement is possible
                if(x_move >= 0 && y_move >= 0 && x_move < 8 && y_move < 8 && board[x][y] != board[x_move][y_move] && board[x_move][y_move] != "BP" 
                && board[x_move][y_move] != "BB" && board[x_move][y_move] != "BN" && board[x_move][y_move] != "BN" && board[x_move][y_move] != "BR" && board[x_move][y_move] != "BK")          
                {
                    // If user destination coordinates are possible then return True
                    if(x_end == x_move && y_end == y_move)
                    {
                        return true;
                    }
                }
                // If there is black game piece in front, then can't go further from it
                if((x_move >= 0 && y_move >= 0 && x_move < 8 && y_move < 8 && board[x][y] != board[x_move][y_move]) && (board[x_move][y_move] == "BP"
                || board[x_move][y_move] == "BB" || board[x_move][y_move] == "BN" || board[x_move][y_move] == "BN" || board[x_move][y_move] == "BR" || board[x_move][y_move] == "BK"))
                {
                    check = 1;
                    // If user destination coordinates are possible then return True
                    if(x_end == x_move && y_end == y_move)
                    {
                        return true;
                    }
                    break;
                }
                // If there is white game piece in front, then you can eat it but not teleport past it
                if((x_move >= 0 && y_move >= 0 && x_move < 8 && y_move < 8 && board[x][y] != board[x_move][y_move]) && (board[x_move][y_move] == "WP"
                || board[x_move][y_move] == "WB" || board[x_move][y_move] == "WN" || board[x_move][y_move] == "WN" || board[x_move][y_move] == "WR" || board[x_move][y_move] == "WK"))
                {
                    check = 1;
                    // If user destination coordinates are possible then return True
                    if(x_end == x_move && y_end == y_move)
                    {
                        return true;
                    }
                    break;
                }
              
            }
        }
        // Can't go further to that direction
        if( check == 1)
        {
            std::cout << "No possible solutions anymore" << std::endl;
        } 
    }
    // If user coordinates have white pawn
    if(board[x][y] == "WP")
    {
        int check = 0;
        int x_move = x;
        int y_move = y;
        // If it is first move
        if(y == 1)
        {
            // If user destination coordinates are possible then return True
            if(x_end == x_move  && y_end == y_move + 2)
            {
                return true;
            }
        }
        // En passant to left 
        if((x > 0 && y > 0 && x < 8 && y< 7) && board[x-1][y] == "BP") 
        {
            // If user destination coordinates are possible then return True
            if(x_end == x_move-1 && y_end == y_move+1)
            {
                board[x-1][y] = " ";
                return true;
            }
        }
        // En passant to right
        if((x >= 0 && y > 0 && x < 7 && y< 7) && board[x+1][y] == "BP") 
        {
            // If user destination coordinates are possible then return True
            if(x_end == x_move+1 && y_end == y_move+1)
            {
                board[x+1][y] = " ";
                return true;
            }
        }
        // If there is something in front
        if((board[x][y+1] == "BP" || board[x][y+1] == "BB"
        || board[x][y+1] == "BN" || board[x][y+1] == "BQ" || board[x][y+1] == "BR" || board[x][y+1] == "BK" || board[x][y+1] == "WP" || board[x][y+1] == "WB"
        || board[x][y+1] == "WN" || board[x][y+1] == "WQ" || board[x][y+1] == "WR" || board[x][y+1] == "WK"))
        {
            check = 1;
        }
        // If there is black game piece at left up column
        if((x > 0 && y >= 0 && x < 8 && y< 7) && (board[x-1][y+1] == "BP" || board[x-1][y+1] == "BB"
        || board[x-1][y+1] == "BN" || board[x-1][y+1] == "BQ" || board[x-1][y+1] == "BR" || board[x-1][y+1] == "BK" ))
        {
            // If user destination coordinates are possible then return True
            if(x_end == x_move-1 && y_end == y_move+1)
            {
                return true;
            }
        }
        // If there is black game piece at right up column
        if((x >= 0 && y > 0 && x < 7 && y< 7) && (board[x+1][y+1] == "BP" || board[x+1][y+1] == "BB"
        || board[x+1][y+1] == "BN" || board[x+1][y+1] == "BQ" || board[x+1][y+1] == "BR" || board[x+1][y+1] == "BK" ))
        {
            // If user destination coordinates are possible then return True
            if(x_end == x_move+1 && y_end == y_move+1)
            {
                return true;
            }
        }
        // There is nothing in front
        if(check == 0)
        {
            // If user destination coordinates are possible then return True
            if(x_end == x_move && y_end == y_move+1)
            {
                return true;
            }
        }
 
    }
    // If user coordinates have black pawn
    if(board[x][y] == "BP")
    {
        int check = 0;
        int x_move = x;
        int y_move = y;
        // If it is first move
        if(y == 6)
        {
            // If user destination coordinates are possible then return True
            if(x_end == x_move && y_end == y_move-2)
            {
                return true;
            }
        }
        // En passant to right
        if((x >= 0 && y > 0 && x < 7 && y< 8) && board[x+1][y] == "WP") 
        {
            // If user destination coordinates are possible then return True
            if(x_end == x_move+1 && y_end == y_move-1)
            {
                board[x+1][y] = " ";
                return true;
            }
        }
        // En passant to left 
        if((x > 0 && y > 0 && x < 8 && y< 8) && board[x-1][y] == "WP") 
        {
            // If user destination coordinates are possible then return True
            if(x_end == x_move-1 && y_end == y_move-1)
            {
                board[x-1][y] = " ";
                return true;
            }
        }
        // If there is something in front
        if((board[x][y-1] == "WP" || board[x][y-1] == "WB"
        || board[x][y-1] == "WN" || board[x][y-1] == "WQ" || board[x][y-1] == "WR" || board[x][y-1] == "WK" || board[x][y-1] == "BP" || board[x][y-1] == "BB"
        || board[x][y-1] == "BN" || board[x][y-1] == "BQ" || board[x][y-1] == "BR" || board[x][y-1] == "BK"))
        {
            check = 1;
        }
        // If there is white game piece at left lower column
        if((x > 0 && y > 0 && x < 8 && y< 8) && (board[x-1][y-1] == "WP" || board[x-1][y-1] == "WB"
        || board[x-1][y-1] == "WN" || board[x-1][y-1] == "WQ" || board[x-1][y-1] == "WR" || board[x-1][y-1] == "WK" ))
        {
            // If user destination coordinates are possible then return True
            if(x_end == x_move-1 && y_end == y_move-1)
            {
                return true;
            }
        }
        // If there is white game piece at right lower column
        if((x >= 0 && y > 0 && x < 7 && y< 8) && (board[x+1][y-1] == "WP" || board[x+1][y-1] == "WB"
        || board[x+1][y-1] == "WN" || board[x+1][y-1] == "WQ" || board[x+1][y-1] == "WR" || board[x+1][y-1] == "WK" ))
        {
            // If user destination coordinates are possible then return True
            if(x_end == x_move+1 && y_end == y_move-1)
            {
                return true;
            }
        }
        // There is nothing in front
        if(check == 0)
        {
            // If user destination coordinates are possible then return True
            if(x_end == x_move && y_end == y_move-1)
            {
                return true;
            }
        }
 
    }
    // Otherwise return false
    
    return false;
    
}