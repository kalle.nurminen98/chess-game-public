#pragma once
#include <fstream>
#include "hps/hps.h"
#include <vector>
#include <iostream>
#include <string>


int saveGame(std::vector<std::vector<std::string>> list_to_be_saved);
int saveFileCheck();
std::vector<std::vector<std::string>> newFile();
std::vector<std::vector<std::string>> loadFile();
