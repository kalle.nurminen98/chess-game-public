#include "promotion.h"

void promotion(int turn, std::string board[][8]){
    std::string promotionPiece;
    
    if(turn%2 != 0){
        for(int i = 0; i < 8; i++){
            if(board[i][0] == "BP"){
                while(true){
                    std::cout << "Do you want to promote to a Queen(Q), Rook(R), Knight(N), or Bishop(B)?" << std::endl;
                    std::cin >> promotionPiece;
                    if(promotionPiece == "Q"){
                        board[i][0] = "BQ";
                        break;
                    }
                    else if(promotionPiece == "R"){
                        board[i][0] = "BR";
                        break;
                    }
                    else if(promotionPiece == "N"){
                        board[i][0] = "BN";
                        break;
                    }
                    else if(promotionPiece == "B"){
                        board[i][0] = "BB";
                        break;
                    }
                    else{
                        std::cout << "Invalid input." << std::endl;
                    }
                }
            }
    }
    }

    else if(turn%2 == 0){
        for(int i = 0; i < 8; i++){
            if(board[i][7] == "WP"){
                while(true){
                    std::cout << "Do you want to promote to a Queen(Q), Rook(R), Knight(N), or Bishop(B)?" << std::endl;
                    std::cin >> promotionPiece;
                    if(promotionPiece == "Q"){
                        board[i][7] = "WQ";
                        break;
                    }
                    else if(promotionPiece == "R"){
                        board[i][7] = "WR";
                        break;
                    }
                    else if(promotionPiece == "N"){
                        board[i][7] = "WN";
                        break;
                    }
                    else if(promotionPiece == "B"){
                        board[i][7] = "WB";
                        break;
                    }
                    else{
                        std::cout << "Invalid input" << std::endl;
                    }
                }
            }
    }
    }
}