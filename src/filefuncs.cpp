// Functions to contain file operations for the save game handling
#include "filefuncs.h"


int saveGame(std::vector<std::vector<std::string>> list_to_be_saved){

	//this function saves vector containing  10 string vectors to a save file in binary format
    std::string serialized;
	
    std::ofstream saveFile;
    saveFile.open("saveFile", std::ios::binary);
	for(int i = 0; i < 10; i++){
		serialized = hps::to_string(list_to_be_saved[i]);
		saveFile << serialized <<std::endl;
	}

    saveFile.close();
    
    
    return 0;

}



int saveFileCheck()// This function checks if save game exists and has 10 lines
{
	std::string filename = "saveFile";   // Name of the file

	std::string line;   // To read each line from code
	int count=0;    // Variable to keep count of each line
	
	std::ifstream mFile (filename);   
	
	if(mFile.is_open()) 
	{
		while(mFile.peek()!=EOF)
		{
			getline(mFile, line);
			count++;
		}
		mFile.close();
		std::cout<<"Savefile succesfully found"<<count<<std::endl;
	}
	else{
        std::cout<<"No save file was found, new save file will be created\n";
		return -1;
        }
		
 
	return count;
}
//Creates a new vector of 10 string vectors if no proper save file exists
std::vector<std::vector<std::string>> newFile(){
	std::vector<std::vector<std::string>> new_holder(10);

	return new_holder;
}

// Loads the entire saveFile into 10 vectors inside a vector
std::vector<std::vector<std::string>> loadFile()
{
	std::vector<std::vector<std::string>> load_holder;
	std::string line;
	std::ifstream saveFile;
	std::string filePath = "saveFile";


	saveFile.open(filePath, std::ios::binary);
	if(saveFile.is_open()){
		std::string dataRead;
		while(getline(saveFile, line))
			{

			load_holder.push_back(hps::from_string<std::vector<std::string>>(line));
			}
		saveFile.close();
		}
	else{
	std::cout << "File read error!" <<std::endl;
	}


	return load_holder;
}

// reads a pgn formatted file and returns first game as a vector

