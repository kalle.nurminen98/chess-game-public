#include "showBoard.h"
#include "getPieces.h"

void showBoard(std::string board[][8]){
    std::string piece;
    std::cout << " ╔══╤══╤══╤══╤══╤══╤══╤══╗" << std::endl;
    for(int j = 7; j >= 0; j--){
        std::cout << 1+j << "║";
        for(int i = 0; i < 8; i++){
            piece = getPieces(board[i][j]);
            if(i == 7){
                std::cout << piece << " ║";
            }
            else{
                std::cout << piece << " │";

            }
        }
        std::cout << std::endl;
        if(j == 0){
            std::cout << " ╚══╧══╧══╧══╧══╧══╧══╧══╝" << std::endl;
        }
        else{
            std::cout << " ╟──┼──┼──┼──┼──┼──┼──┼──╢" << std::endl;
        }
        
    }

    std::cout << "  A  B  C  D  E  F  G  H " << std::endl;
}

std::string getPieces(std::string stringPiece){
    if(stringPiece == "BR"){
        return "♖";
    }
    else if(stringPiece == "BN"){
        return "♘";
    }
    else if(stringPiece == "BB"){
        return "♗";
    }
    else if(stringPiece == "BQ"){
        return "♕"; 
    }
    else if(stringPiece == "BK"){
        return "♔";
    }
    else if(stringPiece == "BP"){
        return "♙";
    }
    else if(stringPiece == "WR"){
        return "♜";
    }
    else if(stringPiece == "WN"){
        return "♞";
    }
    else if(stringPiece == "WB"){
        return "♝";
    }
    else if(stringPiece == "WQ"){
        return "♛"; 
    }
    else if(stringPiece == "WK"){
        return "♚";
    }
    else if(stringPiece == "WP"){
        return "♟";
    }
    else{
        return " ";
    }
}