# Chess game

This is a chessgame.
  
Kalle wrote the code that checks and tells if it is legal movement. Also Kalle did boolean function to only let those movements go to legal coordinates.

Juho made the code that moves the pieces. Juho also wrote the code that allows you to import and save games and most of the code in main.  
Karl wrote the code that sets up the initial board, shows the board and promotes a piece if a pond has moved to the other side of the board.


## Table of Contents

- [Requirements](#requirements)
- [Usage](#usage)
- [Maintainers](#maintainers)
- [Contributing](#contributing)
- [License](#license)

## Requirements

Requires  `gcc`, `make`, `CMake`.

## Usage

To move you must input the coordinate indicating which square you would like to move from followed by the coordinates of the square you would like to move to.  
  
Example (first move):
Input: e2e4  
Output: the pawn moves from e2 to e4.  
  
If the move is illegal the program will not let you move there. Also, you can write "quit" to quit the game or "concede" to give up.  


# List of working features

Saving and loading progess in 10 separate slots and returning to the loaded progress

Pieces are allowed only legal movement patterns

En passant movement(capture works at least sometimes)

Promotion of pawn to queen

# Missing features and bugs
Standard chess notation to moves

Checking for checks, mates, stalemates(game can't be finished)

Pieces of same color are allowed to be captured

Castling

saving of promoted pieces







# Create build directory and move into it
mkdir build && cd build

# Compile the program
make

# Run the compiled binary
./main


## Maintainers

[Juho Kangas] (https://gitlab.com/jhkangas3)  Saving and loading games, gitlab, game structure and loop.
[Kalle Nurminen] (https://gitlab.com/kalle.nurminen98)  
[Karl Munthe] (https://gitlab.com/karl.munthe)

